export class Feed {

    _id?: string;
    title: string;
    body: string;
    img: any
    source: string;
    publisher: string;



    constructor(data?: feed){
        this._id = data && data._id || null;
        this.title = data && data.title || null;
        this.body = data && data.body || null;
        this.source = data && data.source || null;
        this.publisher = data && data.publisher || null;
        this.img = data && data.img || './assets/img/silueta.png' 
    }


}

export interface feed {
    _id?: string,
    title: string,
    body: string,
    img: any,
    source: string,
    publisher: string
}
