import { FeedsComponent } from '../app/feeds/feeds/feeds.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from '../app/dashboard/dashboard/dashboard.component';
import { dashboardRoutesModule } from '../app/dashboard/dashboard/dashboard.routes.module';
import { AuthGuardService } from '../app/auth/auth-guard.service';
import { LoginComponent } from '../app/auth/login/login.component';


const routes: Routes = [
  { path: '', component: DashboardComponent, canActivate: [AuthGuardService], children: dashboardRoutesModule },
  { path: 'home', component: FeedsComponent },
  { path: 'login', component: LoginComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
