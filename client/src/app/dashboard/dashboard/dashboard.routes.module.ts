import { AddComponent } from '../../feeds/add/add.component';
import { Routes } from '@angular/router';
import { ListComponent } from 'src/app/feeds/list/list.component';
import { ViewLinksComponent } from 'src/app/feeds/view-links/view-links.component';



export const dashboardRoutesModule: Routes = [

  { path: 'feed/add', component: AddComponent },
  { path: 'feed/add/:id', component: AddComponent },
  { path: 'feed/list', component: ListComponent },
  { path: 'feed/view/:url', component: ViewLinksComponent },
  { path: '', redirectTo: 'feed/list', pathMatch: 'full' }

];
