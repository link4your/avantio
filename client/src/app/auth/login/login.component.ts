import { AuthService } from '../auth.service';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  email: string;
  password: string;
  error: boolean;
  constructor(private _auth: AuthService, private router: Router) { }

  ngOnInit() {
  }

  onSubmit(form: NgForm) {
    if (form.valid) {
      this.error = false;
      this._auth.login({ 'email': form.value.email, 'password': form.value.password }).subscribe((res: any) => {
        if (!res) {

          this.error = true;
          console.log(this.error)

        } else {

          localStorage.setItem('t_c_a', res.token);
          this.router.navigate(['/']);
        }
      });
    } else {
      this.error = true;
    }
  }

}
