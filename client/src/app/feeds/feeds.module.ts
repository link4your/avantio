import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddComponent } from './add/add.component';
import { ListComponent } from './list/list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { PipesModule } from '../pipes/pipes/pipes.module';
import { FeedsComponent } from './feeds/feeds.component';
import { ViewLinksComponent } from './view-links/view-links.component';







@NgModule({
  declarations: [
    AddComponent,
    ListComponent,
    FeedsComponent,
    ViewLinksComponent

  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    PipesModule
  ],
  exports: [
    AddComponent,
    ListComponent,
    ViewLinksComponent

  ]
})
export class FeedsModule { }
