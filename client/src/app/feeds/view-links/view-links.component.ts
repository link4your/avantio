

import { Component } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { DomSanitizer } from '@angular/platform-browser'

@Component({
  selector: 'app-view-links',
  templateUrl: './view-links.component.html'
})
export class ViewLinksComponent {
  html: any;

  constructor(private route: ActivatedRoute, private sanitizer: DomSanitizer) {
    this.route.params.subscribe(param => {
      if (param.url) {
        this.html = sanitizer.bypassSecurityTrustResourceUrl(param.url);
      }

    });
  }

}






