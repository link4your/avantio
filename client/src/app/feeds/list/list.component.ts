import { Component, OnInit } from '@angular/core';
import { FeedsService } from '../feeds.service';
import { feed } from 'src/app/model/feed.model';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  feeds:feed[] = []; 
  constructor(private _feed: FeedsService) { 
    this._feed.list().subscribe((item: feed[]) => this.feeds = item);
  }

  ngOnInit() {
  }


 delete(_id: string){
   if(_id){
     this._feed.delete(_id).subscribe(item => {
     if(item) {
          this.feeds = this.feeds.filter(item => item._id != _id);
          Swal.fire('Success','the feed was deleted with exist', 'success');
     } else {
      Swal.fire('Alert ','Im sorry, there was a problem', 'warning');
     }
     });
   }
 }

 

}
