
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { feed } from '../model/feed.model';
import { of, Observable } from 'rxjs';
import { mergeMap, catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class FeedsService {

  constructor(private http: HttpClient) { }


  add(feed: feed, file: File) {
    return this.http.post(environment.url + 'feeds', feed).pipe(mergeMap((res: feed) => this.updateImage(res, file)));
  }


  update(feed: feed, file: File) {
    return this.http.put(environment.url + 'feeds', feed).pipe(mergeMap((res: feed) => this.updateImage(res, file)));
  }



  public_list() {
    return this.http.get(environment.url + 'public');
  }

  public_listOne(id: string) {
    return this.http.get(environment.url + 'public/' + id);
  }





  list() {
    return this.http.get(environment.url + 'feeds');
  }

  listOne(id: string) {
    return this.http.get(environment.url + 'feeds/' + id);
  }


  delete(id: string) {
    return this.http.delete(environment.url + 'feeds/' + id);
  }


  updateImage(feed: feed, file: File) {
    if (!file) {
      return of(feed);
    }
    let formData = new FormData();
    formData.append('imagen', file, file.name);

    const header = new Headers();
    let request = new Request(environment.url + 'feeds/' + feed, {
      method: 'PATCH',
      headers: header,
      body: formData
    });
    return fetch(request)

  }


  updateFeed() {
    this.http.get(environment.url + 'feeds/update/feed').subscribe(item => {
      console.log(item);
    });
  }




}
