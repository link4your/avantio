import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Feed, feed } from 'src/app/model/feed.model';
import { NgForm } from '@angular/forms';
import { FeedsService } from '../feeds.service';
import { Router, ActivatedRoute, Routes } from '@angular/router';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {
  data: Feed = new Feed();
  pictureFile: File;
  @ViewChild('image', {  static: true}) image: ElementRef;
  constructor(private _feeds: FeedsService, private router: Router, private route: ActivatedRoute) { 
    this.route.params.subscribe(param => {
      if(param.id){
        this._feeds.listOne(param.id).subscribe((item: Feed) => {
        this.data = new Feed(item);
        })
      }
    })
  }

  ngOnInit() {
  }


  onSubmit(form: NgForm) {
    if(form.valid) {

     if(!this.data._id) {
      this._feeds.add(form.value, this.pictureFile).subscribe(item => {
        
        if(item) {
          console.log('aaaaa')
         this.router.navigate(['/feed/list']);
        } else {
          Swal.fire('Alert ','Im sorry, there was a problem', 'warning');
        }
       });
     } else {
      form.value._id= this.data._id;
      this._feeds.update(form.value, this.pictureFile).subscribe(item => {
        if(item) {
         this.router.navigate(['/feed/list']);
        } else {
          Swal.fire('Alert ','Im sorry, there was a problem', 'warning');
        }
       });

     }

  
    }
  }

 openImage(){
  this.image.nativeElement.click();
 }


  addImage(event){
  const file = event.target.files[0];
  const reader = new FileReader();
  this.pictureFile = file
  reader.readAsDataURL(file);
  reader.onload = (evt) => {
    this.data.img = <string>reader.result;
  }

  }

}
