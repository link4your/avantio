import { Component, OnInit } from '@angular/core';
import { FeedsService } from '../feeds.service';
import { feed } from 'src/app/model/feed.model';


@Component({
  selector: 'app-feeds',
  templateUrl: './feeds.component.html',
  styleUrls: ['./feeds.component.scss']
})
export class FeedsComponent implements OnInit {

  feeds: feed[] = [];
  constructor(private _feed: FeedsService) {
    this._feed.public_list().subscribe((item: feed[]) => this.feeds = item);
  }


  ngOnInit() {
  }

}
