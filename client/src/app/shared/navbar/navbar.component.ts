import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';
import { FeedsService } from 'src/app/feeds/feeds.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  
  constructor(public _auth: AuthService, private _feed: FeedsService) {
    
   }
  
  ngOnInit() {
  }

 

}
