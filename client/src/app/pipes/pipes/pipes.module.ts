import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImagesPipe } from '../images.pipe';
import { DailyImagePipe } from '../daily-image.pipe';





@NgModule({
  declarations: [
    ImagesPipe,
    DailyImagePipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ImagesPipe,
    DailyImagePipe
  ]
})
export class PipesModule { }
