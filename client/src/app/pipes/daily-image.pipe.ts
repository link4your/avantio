import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dailyImage'
})
export class DailyImagePipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    console.log(value)
    if(value === 'default.png') {
      return './assets/img/default.png';
    } else {
    return  value
    }
    

  }

}
