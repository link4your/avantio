import { Pipe, PipeTransform } from '@angular/core';
import { environment } from 'src/environments/environment';


@Pipe({
  name: 'images'
})
export class ImagesPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    if (value) {
      console.log(environment.url + value);
      return environment.url + value;
    } else {
      return './assets/img/silueta.png'
    }
  }

}
