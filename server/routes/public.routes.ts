import { Router, Response, Request } from "express";
import { MongoError, ObjectId } from "mongodb";
import { feed } from "../model/feeds.model";
const auth = require('../middlewares/authToken');
const helper = require('../middlewares/helpers');
const crawler = require('../webscraping/webscraping');


const routes = Router();



routes.get('/', (req: Request, res: Response) => {
    const db = req.app.locals.db;

    crawler.scraper().then((val: any) => {
        let values: feed[] = [];
        values = val;
        db.collection('feeds').find().toArray().then((value: any) => {
            const v = values.concat(value);
            return res.status(200).send(v);
        }).catch((err: MongoError) => {
            return res.status(400).send(false);
        });

    }).catch((err: any) => {

        return res.status(500).send(false);
    })
});



/**
 * test controller
 * 
 * routes.get('/', (req: Request, res: Response) => {

    crawler.scraper().then((val: any) => {
   
        const db = req.app.locals.db;
        db.collection('feeds').find().toArray().then((value: any) => {
            return res.status(200).send(value);
        }).catch((err: MongoError) => {
            return res.status(400).send(false);
        });

    }).catch((err: any) => console.log(err))


});
 * 
 * 
 */








routes.get('/:id', (req: Request, res: Response) => {
    const db = req.app.locals.db;
    const _id = new ObjectId(req.params.id);
    db.collection('feeds').findOne({ _id: _id }).then((value: any) => {
        return res.status(200).send(value);
    }).catch((err: MongoError) => {
        return res.status(400).send(false);
    });
});









export default routes;
