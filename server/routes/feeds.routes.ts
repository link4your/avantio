import { Router, Response, Request } from "express";
import { MongoError, ObjectId } from "mongodb";
import { feed } from "../model/feeds.model";
const auth = require('../middlewares/authToken');
const helper = require('../middlewares/helpers');
const crawler = require('../webscraping/webscraping');


const routes = Router();
const multer = require('multer')
const path = require('path');


const storage = multer.diskStorage({
    destination: function (req: any, file: any, cb: any) {
        cb(null, path.join(__dirname, '../../public'));
    },
    filename: function (req: any, file: any, cb: any) {
        const mime = file.mimetype.split('/');
        const mimetype = mime[1];
        cb(null, file.fieldname + '-' + Date.now() + '.' + mimetype)
    }
});


const upload = multer({
    limits: {
        fileSize: 4 * 1024 * 1024
    },

    storage: storage

})



routes.get('/update/feed', auth.checkAuthorization(['ROLE_ADMINISTRATOR']), (req: Request, res: Response) => {


    crawler.scraper().then((val: any) => {


        return res.status(200).send(val)



    }).catch((err: any) => {

        return res.status(500).send(false);
    })


})






routes.patch('/:id', upload.single('imagen'), (req: any, res: Response) => {

    const db = req.app.locals.db;
    const _id = new ObjectId(req.params.id);



    db.collection('feeds').findOneAndUpdate({ _id: _id }, { $set: { img: req.file.filename } }).then((val: any) => {
        console.log(val)
        return res.status(200).send(true)
    }).catch((err: MongoError) => {
        console.log(err);
        res.status(500).send(false);
    })
});

routes.put('/', auth.checkAuthorization(['ROLE_ADMINISTRATOR']), (req: Request, res: Response) => {
    helper.validateDataForm(['title', 'body', 'source', 'publisher', '_id'], req.body).then((val: any) => {
        const db = req.app.locals.db;
        const body: feed = req.body;
        const _id = new ObjectId(body._id);
        delete body['_id'];
        if (!val) {
            return res.status(200).send(false);
        } else {

            db.collection('feeds').findOneAndUpdate({ _id: _id }, { $set: body }).then((value: any) => {
                return res.status(200).send(_id);
            }).catch((err: MongoError) => {
                console.log(err);
                return res.status(400).send(false);
            });

        }
    }).catch((err: any) => console.log(err));
});


routes.post('/', auth.checkAuthorization(['ROLE_ADMINISTRATOR']), (req: Request, res: Response) => {
    helper.validateDataForm(['title', 'body', 'source', 'publisher'], req.body).then((val: any) => {
        const db = req.app.locals.db;
        const body: feed = req.body;
        delete body['_id'];
        if (!val) {
            return res.status(200).send(false);
        } else {

            db.collection('feeds').insertOne(body).then((val: any) => {
                console.log(val.insertedId)
                return res.status(200).send(val.insertedId);
            }).catch((err: MongoError) => {
                return res.status(400).send(false);
            });

        }
    });
});


routes.get('/', auth.checkAuthorization(['ROLE_ADMINISTRATOR']), (req: Request, res: Response) => {
    const db = req.app.locals.db;

    crawler.scraper().then((val: any) => {
        let values: feed[] = [];
        values = val;
        db.collection('feeds').find().toArray().then((value: any) => {
            const v = values.concat(value);
            return res.status(200).send(v);
        }).catch((err: MongoError) => {
            return res.status(400).send(false);
        });

    }).catch((err: any) => {

        return res.status(500).send(false);
    })
});


routes.get('/:id', auth.checkAuthorization(['ROLE_ADMINISTRATOR']), (req: Request, res: Response) => {
    const db = req.app.locals.db;
    const _id = new ObjectId(req.params.id);
    db.collection('feeds').findOne({ _id: _id }).then((value: any) => {
        return res.status(200).send(value);
    }).catch((err: MongoError) => {
        return res.status(400).send(false);
    });
});


routes.delete('/:id', auth.checkAuthorization(['ROLE_ADMINISTRATOR']), (req: Request, res: Response) => {
    const db = req.app.locals.db;
    const _id = new ObjectId(req.params.id);
    db.collection('feeds').deleteOne({ _id: _id }).then((value: any) => {
        return res.status(200).send(value);
    }).catch((err: MongoError) => {
        return res.status(400).send(false);
    });
});








export default routes;
