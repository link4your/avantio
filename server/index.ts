import Server from "./classes/server";
const express = require('express'); 
import userRouter from './routes/users.routes';
import feedsRouter from './routes/feeds.routes';
import publicRouter from './routes/public.routes';
const bodyParser = require('body-parser');
const cors = require('cors');
const server = new Server();



// Bodyparser
server.app.use(bodyParser.json())
server.app.use(bodyParser.urlencoded({extended: true}));



// CORS
server.app.use(cors({origin: true, credentials: true}));
server.app.use(express.static("public"));



//Rutas
server.app.use('/user',userRouter);
server.app.use('/feeds', feedsRouter);
server.app.use('/public', publicRouter);


// LEVANTAR SERVER
server.start(()=>{
    console.log(`Servidor corriendo en el puerto ${server.port}`);
});
