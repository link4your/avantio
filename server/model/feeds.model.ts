export interface feed {
    _id?: string,
    title: string,
    body: string,
    img: string,
    source: string,
    publisher: string
}
