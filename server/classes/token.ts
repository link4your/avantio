import jwt from 'jsonwebtoken';
const seed = require('../enviroments/enviroments').SECRET;
export default class Token {

    private static caducidad: string = '30d';
    constructor(){}

    static getJwtToken(payload: any): string {
        return jwt.sign({
            user: payload
        }, seed, { expiresIn: this.caducidad});
    }

    static comprobarToken(userToken: string) {
        return new Promise((resolve, reject) =>{
            jwt.verify(userToken, seed, ( err: any, decoded: any) =>{
                if(err) {
                   resolve(false);
                } else {
                    resolve(decoded);
                }
            });
        })

    }


}
