"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const utf8 = require('utf8');
const cheerio = require('cheerio');
const request = require('request');
exports.scraper = () => {
    return new Promise((resolve, reject) => {
        request({
            method: 'GET',
            url: 'https://www.elmundo.es/'
        }, (err, res, body) => {
            if (err)
                return console.error(err);
            let $ = cheerio.load(body, { decodeEntities: false });
            const source = $('title');
            let title = [];
            let url = [];
            let bod = [];
            let img = [];
            $('.ue-c-cover-content__headline-group').each(function (i, elem) {
                if (i < 5) {
                    title[i] = $(elem).text();
                }
            });
            $('.ue-c-cover-content__link').each(function (i, elem) {
                if (i < 5) {
                    url[i] = $(elem).attr('href');
                }
            });
            const b = new Promise((resolve, reject) => {
                let count = 0;
                url.forEach((url, index) => {
                    request({
                        method: 'GET',
                        url: url
                    }, (err, res, body) => {
                        if (err)
                            reject(err);
                        let $ = cheerio.load(body);
                        let image;
                        $('.ue-c-article--first-letter-highlighted').each(function (i, element) {
                            const a = $(element);
                            bod[index] = a.text();
                        });
                        let noexist = false;
                        $('.ue-c-article__media').each(function (i, element) {
                            const a = $(element).children();
                            image = a.attr('src');
                            if (image == undefined) {
                                noexist = true;
                            }
                            else {
                                img[index] = image;
                            }
                        });
                        if (noexist) {
                            $('.ue-c-article__media-img-container').each((i, element) => {
                                const a = $(element).children();
                                image = a.attr('src');
                                img[index] = image;
                                noexist = false;
                            });
                        }
                        count++;
                        if (count === 4) {
                            resolve();
                        }
                    });
                });
            });
            let values = [];
            b.then(() => {
                title.forEach((item, ind) => {
                    if (img[ind] === undefined) {
                        img[ind] = 'default.png';
                    }
                    values.push({ title: item, body: bod[ind], img: img[ind], publisher: url[ind], source: source.text() });
                });
                request({
                    method: 'GET',
                    url: 'https://www.elpais.com/'
                }, (err, res, body) => {
                    if (err)
                        return console.error(err);
                    let $ = cheerio.load(body);
                    const source = $('title');
                    let title = [];
                    let url = [];
                    let bod = [];
                    let img = [];
                    $('.articulo-titulo').each(function (i, elem) {
                        if (i < 5) {
                            title[i] = $(elem).text();
                        }
                    });
                    $('h2.articulo-titulo').each(function (i, elem) {
                        if (i < 5) {
                            url[i] = $(elem).children().attr('href');
                        }
                    });
                    const b = new Promise((resolve, reject) => {
                        let count = 0;
                        url.forEach((url, index) => {
                            request({
                                method: 'GET',
                                url: url
                            }, (err, res, body) => {
                                if (err)
                                    reject(err);
                                let $ = cheerio.load(body);
                                $('#cuerpo_noticia').each(function (i, element) {
                                    const a = $(element);
                                    bod[index] = a.text();
                                });
                                let noexist = false;
                                $('.foto-pie').each(function (i, element) {
                                    let image = $(element).prev().prev().attr('content');
                                    if (image.length < 5) {
                                        count--;
                                    }
                                    else {
                                        img[index] = image;
                                    }
                                });
                                count++;
                                if (count === 4) {
                                    resolve();
                                }
                            });
                        });
                    });
                    b.then(() => {
                        title.forEach((item, ind) => {
                            if (img[ind] === undefined) {
                                img[ind] = 'default.png';
                            }
                            values.push({ title: item, body: bod[ind], img: img[ind], publisher: url[ind], source: source.text() });
                        });
                        resolve(values);
                    });
                });
            });
        });
    });
};
