"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const mongodb_1 = require("mongodb");
const auth = require('../middlewares/authToken');
const helper = require('../middlewares/helpers');
const crawler = require('../webscraping/webscraping');
const routes = express_1.Router();
const multer = require('multer');
const path = require('path');
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.join(__dirname, '../../public'));
    },
    filename: function (req, file, cb) {
        const mime = file.mimetype.split('/');
        const mimetype = mime[1];
        cb(null, file.fieldname + '-' + Date.now() + '.' + mimetype);
    }
});
const upload = multer({
    limits: {
        fileSize: 4 * 1024 * 1024
    },
    storage: storage
});
routes.get('/update/feed', auth.checkAuthorization(['ROLE_ADMINISTRATOR']), (req, res) => {
    crawler.scraper().then((val) => {
        return res.status(200).send(val);
    }).catch((err) => {
        return res.status(500).send(false);
    });
});
routes.patch('/:id', upload.single('imagen'), (req, res) => {
    const db = req.app.locals.db;
    const _id = new mongodb_1.ObjectId(req.params.id);
    db.collection('feeds').findOneAndUpdate({ _id: _id }, { $set: { img: req.file.filename } }).then((val) => {
        console.log(val);
        return res.status(200).send(true);
    }).catch((err) => {
        console.log(err);
        res.status(500).send(false);
    });
});
routes.put('/', auth.checkAuthorization(['ROLE_ADMINISTRATOR']), (req, res) => {
    helper.validateDataForm(['title', 'body', 'source', 'publisher', '_id'], req.body).then((val) => {
        const db = req.app.locals.db;
        const body = req.body;
        const _id = new mongodb_1.ObjectId(body._id);
        delete body['_id'];
        if (!val) {
            return res.status(200).send(false);
        }
        else {
            db.collection('feeds').findOneAndUpdate({ _id: _id }, { $set: body }).then((value) => {
                return res.status(200).send(_id);
            }).catch((err) => {
                console.log(err);
                return res.status(400).send(false);
            });
        }
    }).catch((err) => console.log(err));
});
routes.post('/', auth.checkAuthorization(['ROLE_ADMINISTRATOR']), (req, res) => {
    helper.validateDataForm(['title', 'body', 'source', 'publisher'], req.body).then((val) => {
        const db = req.app.locals.db;
        const body = req.body;
        delete body['_id'];
        if (!val) {
            return res.status(200).send(false);
        }
        else {
            db.collection('feeds').insertOne(body).then((val) => {
                console.log(val.insertedId);
                return res.status(200).send(val.insertedId);
            }).catch((err) => {
                return res.status(400).send(false);
            });
        }
    });
});
routes.get('/', auth.checkAuthorization(['ROLE_ADMINISTRATOR']), (req, res) => {
    const db = req.app.locals.db;
    crawler.scraper().then((val) => {
        let values = [];
        values = val;
        db.collection('feeds').find().toArray().then((value) => {
            const v = values.concat(value);
            return res.status(200).send(v);
        }).catch((err) => {
            return res.status(400).send(false);
        });
    }).catch((err) => {
        return res.status(500).send(false);
    });
});
routes.get('/:id', auth.checkAuthorization(['ROLE_ADMINISTRATOR']), (req, res) => {
    const db = req.app.locals.db;
    const _id = new mongodb_1.ObjectId(req.params.id);
    db.collection('feeds').findOne({ _id: _id }).then((value) => {
        return res.status(200).send(value);
    }).catch((err) => {
        return res.status(400).send(false);
    });
});
routes.delete('/:id', auth.checkAuthorization(['ROLE_ADMINISTRATOR']), (req, res) => {
    const db = req.app.locals.db;
    const _id = new mongodb_1.ObjectId(req.params.id);
    db.collection('feeds').deleteOne({ _id: _id }).then((value) => {
        return res.status(200).send(value);
    }).catch((err) => {
        return res.status(400).send(false);
    });
});
exports.default = routes;
