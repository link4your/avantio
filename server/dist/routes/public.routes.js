"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const mongodb_1 = require("mongodb");
const auth = require('../middlewares/authToken');
const helper = require('../middlewares/helpers');
const crawler = require('../webscraping/webscraping');
const routes = express_1.Router();
routes.get('/', (req, res) => {
    const db = req.app.locals.db;
    crawler.scraper().then((val) => {
        let values = [];
        values = val;
        db.collection('feeds').find().toArray().then((value) => {
            const v = values.concat(value);
            return res.status(200).send(v);
        }).catch((err) => {
            return res.status(400).send(false);
        });
    }).catch((err) => {
        return res.status(500).send(false);
    });
});
/**
 * test controller
 *
 * routes.get('/', (req: Request, res: Response) => {

    crawler.scraper().then((val: any) => {
   
        const db = req.app.locals.db;
        db.collection('feeds').find().toArray().then((value: any) => {
            return res.status(200).send(value);
        }).catch((err: MongoError) => {
            return res.status(400).send(false);
        });

    }).catch((err: any) => console.log(err))


});
 *
 *
 */
routes.get('/:id', (req, res) => {
    const db = req.app.locals.db;
    const _id = new mongodb_1.ObjectId(req.params.id);
    db.collection('feeds').findOne({ _id: _id }).then((value) => {
        return res.status(200).send(value);
    }).catch((err) => {
        return res.status(400).send(false);
    });
});
exports.default = routes;
